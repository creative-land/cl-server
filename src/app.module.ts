import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { ModsModule } from './mods/mods.module';
import { MorganModule } from 'nest-morgan';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RolesModule } from './roles/roles.module';
import { Role } from './roles/entities/role.entity';
import { User } from './users/entities/user.entity';

@Module({
  imports: [
    UsersModule,
    ModsModule,
    MorganModule,
    ConfigModule.forRoot({
      envFilePath: `.${process.env.NODE_ENV}.env`,
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.PG_HOST,
      port: Number(process.env.PG_PORT),
      username: process.env.PG_USERNAME,
      password: process.env.PG_PASSWORD,
      database: process.env.PG_DATABASE,
      entities: [User, Role],
      synchronize: true,
    }),
    RolesModule,
  ],
  controllers: [],
})
export class AppModule {}
