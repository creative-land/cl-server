export class ChangePasswordDto {
  readonly id: number;
  readonly password: string;
}
