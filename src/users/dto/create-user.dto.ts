export class CreateUserDto {
  readonly username: string;
  readonly email: string;
  readonly passwordHash: string;
}
