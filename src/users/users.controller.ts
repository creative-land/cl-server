import { Body, Controller, Get, Post } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Get()
  All() {
    return this.usersService.FindAll();
  }

  @Post()
  Create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.CreateUser(createUserDto);
  }
}
