import { Body } from '@nestjs/common';
import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ChangePasswordDto } from './dto/chnage-password.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
  ) {}

  async CreateUser(dto: CreateUserDto) {
    const user = await this.usersRepository.save(dto);
    return user;
  }

  async FindAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  async ChangePassword(@Body() changePasswordDto: ChangePasswordDto) {}
}
