import { Body, Controller, Get, Post } from '@nestjs/common';
import { CreateRoleDto } from './dto/create-role.dto';
import { Role } from './entities/role.entity';
import { RolesService } from './roles.service';

@Controller('roles')
export class RolesController {
  constructor(private rolesService: RolesService) {}
  @Get()
  All() {
    return this.rolesService.FindAll();
  }

  @Post()
  Create(@Body() dto: CreateRoleDto) {
    return this.rolesService.Create(dto);
  }
}
