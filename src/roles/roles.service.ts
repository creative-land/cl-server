import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateRoleDto } from './dto/create-role.dto';
import { Role } from './entities/role.entity';

@Injectable()
export class RolesService {
  constructor(
    @InjectRepository(Role) private rolesRepository: Repository<Role>,
  ) {}

  async FindAll(): Promise<Role[]> {
    return await this.rolesRepository.find();
  }

  async Create(dto: CreateRoleDto) {
    const role = this.rolesRepository.save(dto);
    return role;
  }
}
