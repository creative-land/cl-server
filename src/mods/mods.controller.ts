import { Controller, Get, Header, Param } from '@nestjs/common';
import { ModsService } from './mods.service';

@Controller('mods')
export class ModsController {
  constructor(private mods: ModsService) {}

  @Get()
  All() {
    const file = this.mods.Files();
    return file;
  }

  @Get('download/:file')
  @Header('Content-type', 'application/jar')
  async Donwload(@Param('file') file: string) {
    return this.mods.FileDownload(file);
  }
}
