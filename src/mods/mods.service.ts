import { Injectable } from '@nestjs/common';
import { readdirSync, readFile } from 'fs';
import { join } from 'path';
import { Mod } from './models/Mod';

@Injectable()
export class ModsService {

    async Files() {
        const directory = process.env.MODS_DIRECTORY;
        const PORT = process.env.PORT;
        const HOST = process.env.HOST || `http://localhost`
        let _files: Array<Mod> = []
        _files = await readdirSync(directory).map((file) => {
            const url = `${HOST}:${PORT}/mods/download/${file}`;
            const name = file.split('-');
            return new Mod(name[0], url);
        });
        return { _files };
    }

    async FileDownload(fileName: string): Promise<Buffer> {
        console.log(fileName);
        const directory = process.env.MODS_DIRECTORY
        const filepath = join(directory, fileName);
        const pdf = await new Promise<Buffer>((resolve, reject) => {
            readFile(filepath, {}, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
        return pdf;
    }

}
