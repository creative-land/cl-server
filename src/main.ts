import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const PORT = process.env.PORT || 5000
  const NODE_ENV = process.env.NODE_ENV || ''
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  await app.listen(PORT, () =>
    console.log(`Server ${NODE_ENV} start [http://localhost:${PORT}]`));
}

bootstrap();
