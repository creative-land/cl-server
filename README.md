## Description

Creative Land API сервер для майнкрафта

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start
#or
$ yarn

# watch mode
$ npm run start:dev
#or
$ yarn start:dev

# production mode
$ npm run start:prod
#or
$ yarn start:prod
```
## License

Nest is [MIT licensed](LICENSE).
